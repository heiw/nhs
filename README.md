#HEIW Form Package
This form package is used by heiw::uxcrudible, but can be used independently too.

###Add package service providers
Add the package service providers to `config/app.php`:
````
/*
 * Package Service Providers...
 */
\Heiw\Nhs\NhsServiceProvider::class,
```` 


###Install NHS data
To install default NHS data (Health Boards, Sites, etc) run the following command:
````
php artisan db:seed --class=Heiw\Nhs\Seeds\DatabaseSeeder
````
 or to automatically run the seeds change ````database\Seeds\DatabaseSeeds.php```` to include the following lines:

````
    public function run()
    {

        $this->call([
            // Add following line:
            \Heiw\Nhs\Seeds\DatabaseSeeder::class,
            
            //// Your other seeders
        ]);
    }

```` 
