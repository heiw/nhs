<?php

namespace Heiw\Nhs\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DisabilityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('disability') ->insertOrIgnore([
            [	'id' => '1', 	'name' => 'Dyslexia/Dyspraxia/ADHD', 	],
            [	'id' => '2', 	'name' => 'Blind/partially sighted', 	],
            [	'id' => '3', 	'name' => 'Deaf/hearing impairment', 	],
            [	'id' => '4', 	'name' => 'Wheelchair user/mobility difficulties', 	],
            [	'id' => '5', 	'name' => 'Autistic Spectrum Disorder/Aspergers Syndrome', 	],
            [	'id' => '6', 	'name' => 'Mental health difficulties', 	],
            [	'id' => '7', 	'name' => 'Unseen disability e.g. diabetes, epilepsy', 	],
            [	'id' => '8', 	'name' => 'Multiple Disabilities', 	],
            [	'id' => '9', 	'name' => 'Disability not listed above', 	],
            [	'id' => '10', 	'name' => 'I do not want to disclose', 	],

        ]);
    }
}
