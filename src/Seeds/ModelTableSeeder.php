<?php

namespace Heiw\Nhs\Seeds;

use Illuminate\Support\Facades\DB;

class ModelTableSeeder extends \Heiw\Uxcrudible\Seeds\ModelTableSeeder
{
    const SITE_ID = 105;
    const HEALTH_BOARD_ID = 108;
    const SPECIALTY_ID = 109;
    const SUB_SPECIALTY_ID = 110;
    const GRADE_ID = 111;
    const NATIONALITY_ID = 112;
    const ORIGIN_ID = 113;
    const DISABILITY_ID = 116;
    const EMPLOYMENT_ID = 117;
    const GENDER_ID = 125;


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $models = [
            ['id' => self::SITE_ID,         'namespace' => 'Heiw\\Nhs\\',  'name' => 'Site'],
            ['id' => self::HEALTH_BOARD_ID, 'namespace' => 'Heiw\\Nhs\\',  'name' => 'HealthBoard'],
            ['id' => self::SPECIALTY_ID,    'namespace' => 'Heiw\\Nhs\\',  'name' => 'Specialty'],
            ['id' => self::SUB_SPECIALTY_ID,'namespace' => 'Heiw\\Nhs\\',  'name' => 'SubSpecialty'],
            ['id' => self::GRADE_ID,        'namespace' => 'Heiw\\Nhs\\',  'name' => 'Grade'],
            ['id' => self::NATIONALITY_ID,  'namespace' => 'Heiw\\Nhs\\',  'name' => 'Nationality'],
            ['id' => self::ORIGIN_ID,       'namespace' => 'Heiw\\Nhs\\',  'name' => 'Origin'],
            ['id' => self::DISABILITY_ID,   'namespace' => 'Heiw\\Nhs\\',  'name' => 'Disability'],
            ['id' => self::EMPLOYMENT_ID,   'namespace' => 'Heiw\\Nhs\\',  'name' => 'Employment'],
            ['id' => self::GENDER_ID,       'namespace' => 'Heiw\\Nhs\\',  'name' => 'Gender'],
        ];

        $model_translations = [
            ['model_id' => self::SITE_ID,         'locale' => 'en', 'display' => 'Site'],
            ['model_id' => self::HEALTH_BOARD_ID, 'locale' => 'en', 'display' => 'Health Board'],
            ['model_id' => self::SPECIALTY_ID,    'locale' => 'en', 'display' => 'Specialty'],
            ['model_id' => self::SUB_SPECIALTY_ID,'locale' => 'en', 'display' => 'Sub Specialty'],
            ['model_id' => self::GRADE_ID,        'locale' => 'en', 'display' => 'Grade'],
            ['model_id' => self::NATIONALITY_ID,  'locale' => 'en', 'display' => 'Nationality'],
            ['model_id' => self::ORIGIN_ID,       'locale' => 'en', 'display' => 'Origin'],
            ['model_id' => self::DISABILITY_ID,   'locale' => 'en', 'display' => 'Disability'],
            ['model_id' => self::EMPLOYMENT_ID,   'locale' => 'en', 'display' => 'Employment'],
            ['model_id' => self::GENDER_ID,       'locale' => 'en', 'display' => 'Gender'],

            ['model_id' => self::SITE_ID,         'locale' => 'cy', 'display' => __('Site', [], 'cy')],
            ['model_id' => self::HEALTH_BOARD_ID, 'locale' => 'cy', 'display' => __('Health Board', [], 'cy')],
            ['model_id' => self::SPECIALTY_ID,    'locale' => 'cy', 'display' => __('Specialty', [], 'cy')],
            ['model_id' => self::SUB_SPECIALTY_ID,'locale' => 'cy', 'display' => __('Sub Specialty', [], 'cy')],
            ['model_id' => self::GRADE_ID,        'locale' => 'cy', 'display' => __('Grade', [], 'cy')],
            ['model_id' => self::NATIONALITY_ID,  'locale' => 'cy', 'display' => __('Nationality', [], 'cy')],
            ['model_id' => self::ORIGIN_ID,       'locale' => 'cy', 'display' => __('Origin', [], 'cy')],
            ['model_id' => self::DISABILITY_ID,   'locale' => 'cy', 'display' => __('Disability', [], 'cy')],
            ['model_id' => self::EMPLOYMENT_ID,   'locale' => 'cy', 'display' => __('Employment', [], 'cy')],
            ['model_id' => self::GENDER_ID,       'locale' => 'cy', 'display' => __('Gender', [], 'cy')],
        ];

        DB::table('model')->insertOrIgnore($models);
        DB::table('model_translation')->insertOrIgnore($model_translations);

        // Add full rights to all models for sys admin.
        foreach($models as $model) {
            \Heiw\Uxcrudible\Seeds\ModelTableSeeder::addFullRightsForSysAdmin($model['id']);
        }
    }
}
