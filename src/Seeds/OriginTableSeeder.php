<?php

namespace Heiw\Nhs\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OriginTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('origin') ->insertOrIgnore([
            [	'id' => '1', 	'name' => 'White (English)', 	],
            [	'id' => '2', 	'name' => 'White (Irish)', 	],
            [	'id' => '3', 	'name' => 'White (Scottish)', 	],
            [	'id' => '4', 	'name' => 'White (Welsh)', 	],
            [	'id' => '5', 	'name' => 'Irish Traveller', 	],
            [	'id' => '6', 	'name' => 'Other White', 	],
            [	'id' => '7', 	'name' => 'Carribean', 	],
            [	'id' => '8', 	'name' => 'African', 	],
            [	'id' => '9', 	'name' => 'Other black background', 	],
            [	'id' => '10', 	'name' => 'Indian', 	],
            [	'id' => '11', 	'name' => 'Pakistani', 	],
            [	'id' => '12', 	'name' => 'Bangladeshi', 	],
            [	'id' => '13', 	'name' => 'Chinese', 	],
            [	'id' => '14', 	'name' => 'Other Asian background', 	],
            [	'id' => '15', 	'name' => 'White and Black Carribean', 	],
            [	'id' => '16', 	'name' => 'White and Black African', 	],
            [	'id' => '17', 	'name' => 'White and Asian', 	],
            [	'id' => '18', 	'name' => 'Other mixed background', 	],
            [	'id' => '19', 	'name' => 'White British', 	],
            [	'id' => '20', 	'name' => 'Black British', 	],
            [	'id' => '21', 	'name' => 'Do not wish to specify', 	],
            [	'id' => '22', 	'name' => 'Other ethnic background', 	],
            [	'id' => '23', 	'name' => 'Asian British', 	],
            [	'id' => '24', 	'name' => 'Any other White background (White)', 	],
        ]);
    }
}
