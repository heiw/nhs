<?php

namespace Heiw\Nhs\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmploymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('employment')->insertOrIgnore([
            ['id' => '1', 'name' => 'Full-time', 'deleted_at' => null,],
            ['id' => '2', 'name' => 'Part-time', 'deleted_at' => '2019-01-01 00:00:00',],
            ['id' => '3', 'name' => 'LTFT', 'deleted_at' => null,],
            ['id' => '4', 'name' => 'Maternity Leave', 'deleted_at' => null,],
            ['id' => '5', 'name' => 'Sick Leave', 'deleted_at' => null,],
            ['id' => '6', 'name' => 'Supernumerary', 'deleted_at' => null,],
            ['id' => '7', 'name' => 'Other', 'deleted_at' => null,],
            ['id' => '8', 'name' => 'Unknown', 'deleted_at' => null,],
        ]);
    }
}
