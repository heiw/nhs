<?php

namespace Heiw\Nhs\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GradeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('grade')->insertOrIgnore([
            [	'id' => '39', 	'code' => '1', 	'name' => '1', 	],
            [	'id' => '40', 	'code' => '2', 	'name' => '2', 	],
            [	'id' => '1', 	'code' => 'ACT', 	'name' => 'Acting Up Consultant', 	],
            [	'id' => '2', 	'code' => 'Cl Fell', 	'name' => 'Clinical Fellow', 	],
            [	'id' => '3', 	'code' => 'CRF', 	'name' => 'Clinical R Fellow', 	],
            [	'id' => '41', 	'code' => 'CRF H', 	'name' => 'CRF H', 	],
            [	'id' => '4', 	'code' => 'CT1', 	'name' => 'CT1', 	],
            [	'id' => '5', 	'code' => 'CT2', 	'name' => 'CT2', 	],
            [	'id' => '6', 	'code' => 'CT3', 	'name' => 'CT3', 	],
            [	'id' => '7', 	'code' => 'DCT', 	'name' => 'Dental Core Training', 	],
            [	'id' => '8', 	'code' => 'DCT1', 	'name' => 'Dental Core Training 1', 	],
            [	'id' => '9', 	'code' => 'DCT2', 	'name' => 'Dental Core Training 2', 	],
            [	'id' => '10', 	'code' => 'DCT3', 	'name' => 'Dental Core Training 3', 	],
            [	'id' => '11', 	'code' => 'DCT4', 	'name' => 'Dental Core Training 4', 	],
            [	'id' => '12', 	'code' => 'DF2', 	'name' => 'Dental F2', 	],
            [	'id' => '13', 	'code' => 'DF3', 	'name' => 'Dental F3', 	],
            [	'id' => '14', 	'code' => 'F1', 	'name' => 'Foundation Post 1', 	],
            [	'id' => '15', 	'code' => 'F2', 	'name' => 'Foundation Post 2', 	],
            [	'id' => '113', 	'code' => 'FTSTA', 	'name' => 'FTSTA', 	],
            [	'id' => '16', 	'code' => 'FTSTA1', 	'name' => 'FTSTA1', 	],
            [	'id' => '17', 	'code' => 'FTSTA2', 	'name' => 'FTSTA2', 	],
            [	'id' => '18', 	'code' => 'GPST', 	'name' => 'GPST', 	],
            [	'id' => '19', 	'code' => 'GPST1', 	'name' => 'GPST1', 	],
            [	'id' => '20', 	'code' => 'GPST2', 	'name' => 'GPST2', 	],
            [	'id' => '21', 	'code' => 'GPST3', 	'name' => 'GPST3', 	],
            [	'id' => '22', 	'code' => 'GPST4', 	'name' => 'GPST4', 	],
            [	'id' => '112', 	'code' => 'LAS', 	'name' => 'LAS', 	],
            [	'id' => '111', 	'code' => 'LAT', 	'name' => 'LAT', 	],
            [	'id' => '23', 	'code' => 'SHO', 	'name' => 'Senior House Officer', 	],
            [	'id' => '24', 	'code' => 'SpR', 	'name' => 'Specialist Registrar', 	],
            [	'id' => '114', 	'code' => 'SpR1', 	'name' => 'SpR1', 	],
            [	'id' => '115', 	'code' => 'SpR2', 	'name' => 'SpR2', 	],
            [	'id' => '25', 	'code' => 'ST1', 	'name' => 'ST1', 	],
            [	'id' => '26', 	'code' => 'ST2', 	'name' => 'ST2', 	],
            [	'id' => '27', 	'code' => 'ST3', 	'name' => 'ST3', 	],
            [	'id' => '28', 	'code' => 'ST3LAT', 	'name' => 'ST3LAT', 	],
            [	'id' => '29', 	'code' => 'ST4', 	'name' => 'ST4', 	],
            [	'id' => '30', 	'code' => 'ST4LAT', 	'name' => 'ST4LAT', 	],
            [	'id' => '31', 	'code' => 'ST5', 	'name' => 'ST5', 	],
            [	'id' => '32', 	'code' => 'ST6', 	'name' => 'ST6', 	],
            [	'id' => '33', 	'code' => 'ST7', 	'name' => 'ST7', 	],
            [	'id' => '34', 	'code' => 'ST8', 	'name' => 'ST8', 	],
            [	'id' => '35', 	'code' => 'StRh', 	'name' => 'StRh', 	],
            [	'id' => '36', 	'code' => 'StRL', 	'name' => 'StRL', 	],
            [	'id' => '37', 	'code' => 'TFELL', 	'name' => 'Teaching Fellow', 	],
            [	'id' => '38', 	'code' => 'TrDr', 	'name' => 'Trust Doctor', 	],
            [	'id' => '116', 	'code' => 'SpR3', 	'name' => 'SpR3', 	],
            [	'id' => '117', 	'code' => 'SpR4', 	'name' => 'SpR4', 	],
            [	'id' => '118', 	'code' => 'SpR5', 	'name' => 'SpR5', 	],
            [	'id' => '119', 	'code' => 'Consultant', 	'name' => 'Consultant', 	],
            [	'id' => '120', 	'code' => 'Locum', 	'name' => 'Locum', 	],
            [	'id' => '121', 	'code' => 'SAS', 	'name' => 'SAS', 	],
            [	'id' => '122', 	'code' => 'Trade Grade', 	'name' => 'Trade Grade', 	],
            [	'id' => '124', 	'code' => 'Unemployed', 	'name' => 'Unemployed', 	],
            [	'id' => '125', 	'code' => 'Refugee Doctor', 	'name' => 'Refugee Doctor', 	],
            [	'id' => '126', 	'code' => 'Returner to Medicine', 	'name' => 'Returner to Medicine', 	],
            [	'id' => '127', 	'code' => 'SpT', 	'name' => 'SpT', 	],
            [	'id' => '128', 	'code' => 'Unknown', 	'name' => 'Unknown', 	],
            [	'id' => '129', 	'code' => 'GP', 	'name' => 'GP', 	],
            [	'id' => '1032', 	'code' => 'DF1', 	'name' => 'DF1', 	],
            [	'id' => '1033', 	'code' => 'SpD', 	'name' => 'SpD', 	],
            [	'id' => '1034', 	'code' => 'CT2LAT', 	'name' => 'CT2LAT', 	],
            [	'id' => '1039', 	'code' => 'SpR6', 	'name' => 'SpR6', 	],
            [	'id' => '1045', 	'code' => 'F3', 	'name' => 'F3', 	],
            [	'id' => '1046', 	'code' => 'IMT 1', 	'name' => 'IMT 1', 	],
            [	'id' => '1047', 	'code' => 'IMY1', 	'name' => 'IMY1', 	],
            [	'id' => '1048', 	'code' => 'IMY2', 	'name' => 'IMY2', 	],
            [	'id' => '1049', 	'code' => 'IMY3', 	'name' => 'IMY3', 	],
        ]);
    }
}
