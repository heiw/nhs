<?php

namespace Heiw\Nhs\Seeds;

use Heiw\Uxcrudible\Seeds\RoleTableSeeder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PageTableSeeder extends \Heiw\Uxcrudible\Seeds\PageTableSeeder
{
    const NHS_ID = 106;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $pages = [
            [
                'id' => self::NHS_ID,
                'tree_order' => '/1/3',
                'display_order' => '3',
                'page_type' => '',
                'icon' => 'fas fa-medkit',
                'parent_id' => '2',
                'public' => '0',
            ],
        ];
        $page_translation = [
            [
                'page_id' => self::NHS_ID,
                'path' => '/admin/nhs',
                'name' => 'NHS',
                'slug' => 'nhs',
                'title' => '',
                'description' => '',
                'content' => '',
                'locale' => 'en',
            ],
            [
                'page_id' => self::NHS_ID,
                'path' => '/gweinyddiaeth/gig',
                'name' => 'GIG',
                'slug' => 'gig',
                'title' => '',
                'description' => '',
                'content' => '',
                'locale' => 'cy',
            ],
        ];


        DB::table('page')->insertOrIgnore($pages);
        DB::table('page_translation')->insertOrIgnore($page_translation);

        // Add full rights to all pages for sys admin and admin.
        foreach ($pages as $page) {
            DB::table('page_role')->insertOrIgnore([
                ['page_id' => $page['id'], 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::SYS_ADMIN)],
                ['page_id' => $page['id'], 'role_id' => RoleTableSeeder::idFromName(RoleTableSeeder::ADMIN)],
            ]);
        }

        foreach (
            [
                ModelTableSeeder::SITE_ID,
                ModelTableSeeder::HEALTH_BOARD_ID,
                ModelTableSeeder::SPECIALTY_ID,
                ModelTableSeeder::SUB_SPECIALTY_ID,
                ModelTableSeeder::GRADE_ID,
                ModelTableSeeder::NATIONALITY_ID,
                ModelTableSeeder::ORIGIN_ID,
                ModelTableSeeder::DISABILITY_ID,
                ModelTableSeeder::EMPLOYMENT_ID,
                ModelTableSeeder::GENDER_ID
            ] as $modelId) {
            \Heiw\Uxcrudible\Seeds\PageTableSeeder::addModelToPage($modelId, PageTableSeeder::NHS_ID);
        }
    }
}
