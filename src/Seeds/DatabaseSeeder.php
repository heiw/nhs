<?php

namespace Heiw\Nhs\Seeds;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            DisabilityTableSeeder::class,
            EmploymentTableSeeder::class,
            GenderTableSeeder::class,
            GradeTableSeeder::class,
            HealthBoardTableSeeder::class,
            NationalityTableSeeder::class,
            OriginTableSeeder::class,
            SiteTableSeeder::class,
            SpecialtyTableSeeder::class,
            SubSpecialtyTableSeeder::class,

            ModelTableSeeder::class,
            PageTableSeeder::class,
        ]);
    }
}
