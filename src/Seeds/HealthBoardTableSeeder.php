<?php

namespace Heiw\Nhs\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HealthBoardTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('health_board')->insertOrIgnore([
            ['id' => '1', 'code' => '7A1', 'name' => 'Betsi Cadwaladr University LHB',],
            ['id' => '2', 'code' => '7A2', 'name' => 'Hywel Dda University LHB',],
            ['id' => '3', 'code' => '7A3', 'name' => 'Swansea Bay University LHB',],
            ['id' => '4', 'code' => '7A4', 'name' => 'Cardiff & Vale University LHB',],
            ['id' => '5', 'code' => '7A5', 'name' => 'Cwm Taf University LHB',],
            ['id' => '6', 'code' => '7A6', 'name' => 'Aneurin Bevan University LHB',],
            ['id' => '7', 'code' => '7A7', 'name' => 'Powys Teaching LHB',],
            ['id' => '8', 'code' => 'RYT', 'name' => 'Public Health Wales',],
            ['id' => '9', 'code' => 'RQF', 'name' => 'Velindre NHS Trust',],
            ['id' => '10', 'code' => 'RT4', 'name' => 'Welsh Ambulance Service NHS Trust',],
            ['id' => '11', 'code' => '0O0', 'name' => 'Other',],
        ]);
    }
}
