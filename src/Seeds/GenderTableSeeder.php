<?php

namespace Heiw\Nhs\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class GenderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('gender')->insertOrIgnore([
            ['id' => '1', 'name' => 'Female'],
            ['id' => '2', 'name' => 'Male'],

        ]);
    }
}
