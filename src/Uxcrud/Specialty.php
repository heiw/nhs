<?php

namespace Heiw\Nhs\Uxcrud;

use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\HasMany;
use Heiw\Uxcrudible\Form\Fields\HasOne;
use Heiw\Uxcrudible\Form\Fields\Text;
use Heiw\Uxcrudible\UxcrudController;

class Specialty extends UxcrudController
{
    public static $model = \Heiw\Nhs\Models\Specialty::class;


    public function fields($model = null)
    {
        $fields = [
            Text::create('code', __('Code'))
                ->icon('fas fa-asterisk')
                ->validation('required')
            ,
            Text::create('name', __('Name'))
                ->icon('fas fa-tag')
                ->validation('required')
            ,
            HasMany::create('subSpecialties', __('Sub specialties'))
                ->setModel(\Heiw\Nhs\Models\SubSpecialty::class)
                ->addPrependOption('', '-')
            ,
            Actions::create()
        ];
        return $fields;
    }

}
