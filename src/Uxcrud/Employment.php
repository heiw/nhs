<?php

namespace Heiw\Nhs\Uxcrud;

use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\UxcrudController;
use Heiw\Uxcrudible\Form\Fields\Text;

class Employment extends UxcrudController
{
    public static $model = \Heiw\Nhs\Models\Employment::class;


    public function fields($model = null)
    {
        $fields = [
            Text::create('name', __('Name'))
                ->icon('fas fa-tag')
                ->validation('required')
            ,
            Actions::create()
        ];
        return $fields;
    }
}
