<?php

namespace Heiw\Nhs\Uxcrud;

use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\HasOne;
use Heiw\Uxcrudible\UxcrudController;
use Heiw\Uxcrudible\Form\Fields\Text;
use Heiw\Uxcrudible\Form\Fields\Select;
use Illuminate\Http\Request;

class Site extends UxcrudController
{
    public static $model = \Heiw\Nhs\Models\Site::class;


    public function fields($model = null)
    {
        $fields = [
            Text::create('code', __('Code'))
                ->icon('fas fa-asterisk')
                ->validation('required|min:6|max:6')
            ,
            Text::create('name', __('Name'))
                ->icon('fas fa-tag')
                ->validation('required')
            ,
            HasOne::create('health_board', __('Health board'))
                ->labelName('summary()')
                ->setModel(\Heiw\Nhs\Models\HealthBoard::class)
                ->addPrependOption('', __('-'))
                ->validation('required')
            ,
            Actions::create()
        ];
        return $fields;
    }
}
