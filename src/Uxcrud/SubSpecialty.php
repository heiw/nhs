<?php

namespace Heiw\Nhs\Uxcrud;

use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\HasMany;
use Heiw\Uxcrudible\Form\Fields\Text;
use Heiw\Uxcrudible\UxcrudController;

class SubSpecialty extends UxcrudController
{
    public static $model = \Heiw\Nhs\Models\SubSpecialty::class;

    public function fields($model = null)
    {
        $fields = [
            Text::create('code', __('Code'))
                ->icon('fas fa-asterisk')
                ->validation('required')
            ,
            Text::create('name', __('Name'))
                ->icon('fas fa-tag')
                ->validation('required')
            ,
            HasMany::create('specialties', __('Specialties'))
                ->setModel(\Heiw\Nhs\Models\Specialty::class)
                ->addPrependOption('', '-')
            ,
            Actions::create()
        ];
        return $fields;
    }

}
