<?php

namespace Heiw\Nhs\Uxcrud;

use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\Form\Fields\HasOne;
use Heiw\Uxcrudible\UxcrudController;
use Heiw\Uxcrudible\Form\Fields\Text;
use Heiw\Uxcrudible\Form\Fields\Select;
use Illuminate\Http\Request;

class Gender extends UxcrudController
{
    public static $model = \Heiw\Nhs\Models\Gender::class;


    public function fields($model = null)
    {
        $fields = [
            Text::create('name', __('Name'))
                ->icon('fas fa-tag')
            ,
            Actions::create()
        ];
        return $fields;
    }
}
