<?php

namespace Heiw\Nhs\Uxcrud;

use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\UxcrudController;
use Heiw\Uxcrudible\Form\Fields\Text;

class Grade extends UxcrudController
{
    public static $model = \Heiw\Nhs\Models\Grade::class;


    public function fields($model = null)
    {
        $fields = [
            Text::create('code', __('Code'))
                ->icon('fas fa-asterisk')
                ->validation('required')
            ,
            Text::create('name', __('Name'))
                ->icon('fas fa-tag')
                ->validation('required')
            ,
            Actions::create()
        ];
        return $fields;
    }
}
