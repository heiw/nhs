<?php

namespace Heiw\Nhs\Uxcrud;

use Heiw\Uxcrudible\Form\Fields\Actions;
use Heiw\Uxcrudible\UxcrudController;
use Heiw\Uxcrudible\Form\Fields\Text;
use Heiw\Uxcrudible\Form\Fields\HasMany;

class HealthBoard extends UxcrudController
{
    public static $model = \Heiw\Nhs\Models\HealthBoard::class;


    public function fields($model = null)
    {
        $fields = [
            Text::create('code', __('Code'))
                ->icon('fas fa-asterisk')
                ->validation('required')
            ,
            Text::create('name', __('Name'))
                ->icon('fas fa-tag')
                ->validation('required')
            ,
            HasMany::create('sites')
                //->labelName('summary()')
                ->setModel(\Heiw\Nhs\Models\Site::class)
                ->hideOnIndex()
            ,
            Actions::create()
        ];
        return $fields;
    }
}
