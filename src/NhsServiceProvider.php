<?php

namespace Heiw\Nhs;

use Illuminate\Support\ServiceProvider;
use App;

class NhsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'core');
        $this->loadJsonTranslationsFrom(__DIR__ . '/resources/lang');
        $this->loadViewsFrom(__DIR__.'/resources/views', 'core');

        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }
}
