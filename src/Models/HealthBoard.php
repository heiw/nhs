<?php

namespace Heiw\Nhs\Models;

use Heiw\Uxcrudible\Traits\RelationsManager;
use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Database\Eloquent\Model;

class HealthBoard extends UxcrudModel
{
    protected $table = 'health_board';
    protected static $orderBy = 'name';
    public static $icon = 'fas fa-city';

    protected $fillable = ['code', 'name'];

    public static $validationRules = [
        'code' => 'required|min:3|max:3',
        'name' => 'required'
    ];

    public function summary() {
        return "[" . $this->code . "] " . $this->name;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sites() : \Illuminate\Database\Eloquent\Relations\HasMany {
        return $this->hasMany(Site::class)->orderBy(HealthBoard::$orderBy);
    }

//    public function allSites() {
//        return Site::orderBy('name')->get();
//    }
}
