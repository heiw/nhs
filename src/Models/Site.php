<?php

namespace Heiw\Nhs\Models;

use Heiw\Uxcrudible\Traits\RelationsManager;
use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Database\Eloquent\Model;

class Site extends UxcrudModel
{
    protected $table = 'site';
    protected static $orderBy = 'name';
    public static $icon = 'far fa-hospital';

    protected $fillable = ['code', 'name', 'health_board_id'];

    public function summary() {
        return "[" . $this->code . "] " . $this->name;
    }

    public function healthBoard() {
        return $this->belongsTo(HealthBoard::class)->orderBy(HealthBoard::getOrderBy());
    }

//    public function allHealthBoards() {
//        return HealthBoard::orderBy(static::getOrderBy())->get();
//    }
}
