<?php

namespace Heiw\Nhs\Models;

use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Database\Eloquent\Model;

class Employment extends UxcrudModel
{
    protected $table = 'employment';
    protected static $orderBy = 'name';
    public static $icon = 'fas fa-user-tie';

    protected $fillable = ['name'];
}
