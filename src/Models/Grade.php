<?php

namespace Heiw\Nhs\Models;

use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Database\Eloquent\Model;

class Grade extends UxcrudModel
{
    protected $table = 'grade';
    protected static $orderBy = 'name';
    public static $icon = 'fas fa-signal';

    protected $fillable = ['code', 'name'];
}
