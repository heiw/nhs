<?php

namespace Heiw\Nhs\Models;

use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Database\Eloquent\Model;

class Origin extends UxcrudModel
{
    protected $table = 'origin';
    protected static $orderBy = 'name';
    public static $icon = 'fas fa-globe';

    protected $fillable = ['name'];
}
