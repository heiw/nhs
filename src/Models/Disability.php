<?php

namespace Heiw\Nhs\Models;

use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Database\Eloquent\Model;

class Disability extends UxcrudModel
{
    protected $table = 'disability';
    protected static $orderBy = 'name';
    public static $icon = 'fab fa-accessible-icon';

    protected $fillable = ['name'];
}
