<?php

namespace Heiw\Nhs\Models;

use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Database\Eloquent\Model;

class SubSpecialty extends UxcrudModel
{
    protected $table = 'sub_specialty';
    protected static $orderBy = 'name';
    public static $icon = 'fas fa-cubes';

    protected $fillable = ['code', 'name', 'specialties[]'];

    protected static $validationRules = [
        'code' => 'required',
        'name' => 'required',
        'specialties[]' => 'nullable'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function specialties() : \Illuminate\Database\Eloquent\Relations\BelongsToMany {
        return $this->belongsToMany(Specialty::class)->orderBy(Specialty::getOrderBy());
    }

    public function specialtyIds() {
        return $this->specialties()->pluck('id')->keyBy('id');
    }

//    public function allSpecialties() {
//        return Specialty::orderBy(static::getOrderBy())->get();
//    }

}
