<?php

namespace Heiw\Nhs\Models;

use Heiw\Uxcrudible\Traits\RelationsManager;
use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Database\Eloquent\Model;

class Gender extends UxcrudModel
{
    protected $table = 'gender';
    protected static $orderBy = 'name';
    public static $icon = 'fas fa-transgender-alt';

    protected $fillable = ['name'];

    protected static $validationRules = [
        'name' => 'required',
    ];
}
