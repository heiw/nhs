<?php

namespace Heiw\Nhs\Models;

use Heiw\Uxcrudible\UxcrudModel;

class Specialty extends UxcrudModel
{
    protected $table = 'specialty';
    protected static $orderBy = 'name';
    public static $icon = 'fas fa-cube';

    protected $fillable = ['code', 'name', 'subSpecialties[]'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function subSpecialties() : \Illuminate\Database\Eloquent\Relations\BelongsToMany {
        return $this->belongsToMany(SubSpecialty::class)->orderBy(SubSpecialty::getOrderBy());
    }

    public function subSpecialtyIds() {
        return $this->subSpecialties()->pluck('id')->keyBy('id');
    }


//    public function allSubSpecialties() {
//        return SubSpecialty::orderBy(static::getOrderBy())->get();
//    }

}
