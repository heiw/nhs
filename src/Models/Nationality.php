<?php

namespace Heiw\Nhs\Models;

use Heiw\Uxcrudible\UxcrudModel;
use Illuminate\Database\Eloquent\Model;

class Nationality extends UxcrudModel
{
    protected $table = 'nationality';
    protected static $orderBy = 'name';
    public static $icon = 'fas fa-passport';

    protected $fillable = ['name'];

    protected static $validationRules = [
        'name' => 'required',
    ];
}

