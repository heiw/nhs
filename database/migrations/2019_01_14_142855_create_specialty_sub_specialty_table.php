<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateSpecialtySubSpecialtyTable
 */
class CreateSpecialtySubSpecialtyTable extends Migration
{
    /**
     * Run the migrations.
     * Create many to many link table between specialty and sub_specialty.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specialty_sub_specialty', function (Blueprint $table) {
            $table->unsignedBigInteger('specialty_id');
            $table->unsignedBigInteger('sub_specialty_id');

            $table->foreign('specialty_id')
                ->references('id')
                ->on('specialty')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('sub_specialty_id')
                ->references('id')
                ->on('sub_specialty')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->primary(['specialty_id', 'sub_specialty_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specialty_sub_specialty');
    }
}
