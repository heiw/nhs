<?php

use Faker\Generator as Faker;

$factory->define(\Heiw\Nhs\Models\SubSpecialty::class, function (Faker $faker) {
    $name = trim($faker->sentence($nbWords = 3, $variableNbWords = true), '.');
    $code = strtoupper(substr($name, 0, 1));
    return [
        'code' => $faker->unique()->numerify($code . '##'),
        'name' => $name,
    ];
});
