<?php

use Faker\Generator as Faker;

$factory->define(\Heiw\Nhs\Models\Site::class, function (Faker $faker) {
    $companyName = $faker->company;
    $code = strtoupper(substr($companyName, 0, 2));
    return [
        'code' => $faker->unique()->numerify($code . '####'),
        'name' => $companyName,
        'type_id' => $faker->randomElement(['H', 'P']),
        'health_board_id' => $faker->randomElement(App\HealthBoard::pluck('id', 'id')->toArray())
        /*
        'health_board_id' => function (array $health_board) {
            return App\HealthBoard::find()
        }*/
    ];
});
