<?php

use Faker\Generator as Faker;

$factory->define(\Heiw\Nhs\Models\Specialty::class, function (Faker $faker) {
    $name = trim($faker->sentence($nbWords = 3, $variableNbWords = true), '.');
    return [
        'code' => $faker->unique()->numerify('###'),
        'name' => $name,
    ];
});
