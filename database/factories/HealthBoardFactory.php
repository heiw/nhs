<?php

use Faker\Generator as Faker;

$factory->define(\Heiw\Nhs\Models\HealthBoard::class, function (Faker $faker) {
    $companyName = $faker->company;
    $code = strtoupper(substr($companyName, 0, 2));
    return [
        'code' => $faker->unique()->numerify($code . '#'),
        'name' => $companyName,
    ];
});
