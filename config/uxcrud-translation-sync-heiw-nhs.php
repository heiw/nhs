<?php
/*
 * To run command
 * php artisan uxcrud:translation-sync --options=config/uxcrud-translation-sync-heiw-nhs.php
 */

return [
    'locale' => 'cy',

    'namespace' => 'Heiw.Nhs',

    // For packages set to true to copy before and after translatable command.
    'copyFirstFile' => true,

    // List of files to export. Ensure the first file is the main file when using copyFirstFile
    'files' => [
        'packages/heiw/nhs/src/resources/lang/cy.json' => 0,
    ]
];
